/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ifis.analogyvectors;

import java.io.File;
import java.util.List;
import org.apache.uima.resource.ResourceInitializationException;
import org.deeplearning4j.models.word2vec.Word2Vec;
import org.deeplearning4j.models.word2vec.wordstore.VocabCache;
import org.deeplearning4j.util.SerializationUtils;
import org.nd4j.linalg.api.ndarray.INDArray;
import org.springframework.cache.ehcache.EhCacheCache;

/**
 *
 * @author lofi
 */
public class SimpleSerializedLoader {

    public static void main(String[] args) throws ResourceInitializationException {
        Word2Vec vectors = SerializationUtils.readObject(new File(Config.getDataPath() + "politicalSpeeches/speeches.word2vec"));
        VocabCache cache = SerializationUtils.readObject(new File(Config.getDataPath() + "politicalSpeeches/speeches.word2vec_cache"));
        vectors.setCache(cache);

        cache.words().stream().forEach((s) -> {
            System.out.println(s);
        });
        System.out.println("Plotting...");
        INDArray a = vectors.getWordVectorMatrix("sputnik");
        System.out.println(a.toString());
        vectors.plotTsne();

    }

}
