/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ifis.analogyvectors;

import java.io.IOException;
import java.util.List;
import org.apache.uima.resource.ResourceInitializationException;
import org.deeplearning4j.models.word2vec.Word2Vec;
import org.deeplearning4j.models.word2vec.loader.Word2VecLoader;

/**
 *
 * @author Christoph
 */
public class GoogleLoader {

    private static void loadGoogle() throws IOException {
        Word2Vec w2v = Word2VecLoader.loadGoogleModel(Config.getDataPath()+"word2vec_google/GoogleNews-vectors-negative300.bin", true);
        List<String> results = w2v.analogyWords("house", "door", "ship");
        results.forEach((word) -> System.out.print(word + "; "));
    }

    public static void main(String[] args) throws IOException {
        loadGoogle();
    }
    
}
