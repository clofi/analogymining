/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ifis.analogyvectors;

import java.io.File;
import org.apache.uima.resource.ResourceInitializationException;
import org.deeplearning4j.models.word2vec.Word2Vec;
import org.deeplearning4j.models.word2vec.wordstore.VocabCache;
import org.deeplearning4j.models.word2vec.wordstore.inmemory.InMemoryLookupCache;
import org.deeplearning4j.plot.Tsne;
import org.deeplearning4j.text.inputsanitation.InputHomogenization;
import org.deeplearning4j.text.sentenceiterator.FileSentenceIterator;
import org.deeplearning4j.text.sentenceiterator.SentenceIterator;
import org.deeplearning4j.text.sentenceiterator.SentencePreProcessor;
import org.deeplearning4j.text.tokenization.tokenizerfactory.TokenizerFactory;
import org.deeplearning4j.text.tokenization.tokenizerfactory.UimaTokenizerFactory;
import org.deeplearning4j.util.SerializationUtils;

/**
 *
 * @author Christoph
 */
public class SimpleTxtBuilder {

    private static void parseWikipedia() throws ResourceInitializationException {
        File folder = new File(Config.getDataPath()+"politicalSpeeches/rawtxt");

        // setup text source
        SentenceIterator sentenceIter = new FileSentenceIterator((String sentence) -> new InputHomogenization(sentence).transform(), folder);
        TokenizerFactory tokenizer = new UimaTokenizerFactory();
        Word2Vec vectors = new Word2Vec.Builder().windowSize(5).layerSize(300).iterate(sentenceIter).tokenizerFactory(tokenizer).build();
        // setup cache
        //
        vectors.fit();
        
        SerializationUtils.saveObject(vectors, new File(Config.getDataPath()+"politicalSpeeches/speeches.word2vec"));
        SerializationUtils.saveObject(vectors.getCache(), new File(Config.getDataPath()+"politicalSpeeches/speeches.word2vec_cache"));
        
        vectors.plotTsne();
        
        
    }

    public static void main(String[] args) throws ResourceInitializationException {
        parseWikipedia();
    }

}
