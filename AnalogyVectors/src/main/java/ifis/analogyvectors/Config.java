/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ifis.analogyvectors;

import com.typesafe.config.ConfigFactory;

/**
 *
 * @author lofi
 */
public class Config {

    static com.typesafe.config.Config config = ConfigFactory.load();

    public static String getDataPath() {
        return config.getString("analogyvectors.datapath");
    }

}
